const jwt = require("jsonwebtoken")
const config = require("config.js")

const responses = {
  NO_TOKEN: "noToken",
  TOKEN_INVALID: "tokenInvalid",
}

const checkToken = (req, res, next) => {
  const token = req.headers?.authorization?.split(" ")?.[1]
  if (!token) return res.status(403).json({ message: responses.NO_TOKEN })

  return jwt.verify(token, config.jwt.secret, (err, decoded) => {
    if (err) {
      return res.status(403).json({ message: responses.TOKEN_INVALID })
    }

    req.decoded = decoded
    return next()
  })
}

module.exports = {
  checkToken,
  responses,
}
