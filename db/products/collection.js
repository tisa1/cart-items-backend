const mongoose = require("mongoose")
const { ProductSchema } = require("./schema")

const Product = mongoose.model("products", ProductSchema)

module.exports = {
  Product,
}
