const mongoose = require("mongoose")

// Mongoose Schema for Products
const ProductSchema = new mongoose.Schema({
  subcategoryId: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  organisationId: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  data: mongoose.Mixed, // used for collecting the template data
}, { timestamps: true })

module.exports = {
  ProductSchema,
}
