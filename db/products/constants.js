const responses = {
  PRODUCT_CREATED: "productCreated",

  NO_ORGANISATION_ID: "noOrganisationId",
  ORGANISATION_ID_INVALID: "organisationIdInvalid",
  NO_ORGANISATION: "noOrganisation",
  NO_ORGANISATION_MEMBERSHIP: "noOrganisationMembership",

  NO_SUBCATEGORY_ID: "noSubcategoryId",
  SUBCATEGORY_ID_INVALID: "subcategoryIdInvalid",
  NO_SUBCATEGORY: "noSubcategory",

  // template validation
  MISSING_TEMPLATE_FIELD: "missingTemplateField",
  INVALID_TEMPLATE_FIELD: "invalidTemplateField",
}

module.exports = {
  responses,
}
