const mongoose = require("mongoose")
const User = require("db/users/collection")

describe("auth-methods", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await User.deleteMany({})
  })

  it("should create an user", async () => {
    const data = {
      profile: {
        firstName: "John",
        lastName: "Doe",
      },
    }

    await new User(data).save()
    const num = await User.countDocuments()
    expect(num).toBe(1)
  })

  it("should NOT let you insert an empty user", async () => {
    const fakeUser = {}
    let error = null
    try {
      await new User(fakeUser).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })
})
