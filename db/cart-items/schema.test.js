const mongoose = require("mongoose")
const { CartItem } = require("db/cart-items/collection")

describe("cart-items", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await CartItem.deleteMany({})
  })

  it("should create a cart-item", async () => {
    const data = {
      userId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      productId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      quantity: 2,
    }

    await new CartItem(data).save()
    const num = await CartItem.countDocuments()
    expect(num).toBe(1)
  })

  it("should NOT let you insert a cart item without userId", async () => {
    const fakeCartItem = {}
    let error = null
    try {
      await new CartItem(fakeCartItem).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })

  it("should NOT let you insert a cart item without productId", async () => {
    const fakeCartItem = {
      userId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
    }
    let error = null
    try {
      await new CartItem(fakeCartItem).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })

  it("should NOT let you insert a cart item without quantity", async () => {
    const fakeCartItem = {
      userId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      productId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
    }
    let error = null
    try {
      await new CartItem(fakeCartItem).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })

  it("should NOT let you insert a cart item with a non-integer quantity", async () => {
    const fakeCartItem = {
      userId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      productId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      quantity: -1,
    }
    let error = null
    try {
      await new CartItem(fakeCartItem).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })

  it("should NOT let you insert a cart item with a non-integer quantity", async () => {
    const fakeCartItem = {
      userId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      productId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      quantity: 5.4,
    }
    let error = null
    try {
      await new CartItem(fakeCartItem).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })

  it("should NOT let you insert a cart item with a zero quantity", async () => {
    const fakeCartItem = {
      userId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      productId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      quantity: 0,
    }
    let error = null
    try {
      await new CartItem(fakeCartItem).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })
})
