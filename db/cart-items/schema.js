const mongoose = require("mongoose")

// Mongoose Schema for CartItem
const CartItemSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  productId: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
    validate: {
      validator(v) {
        return Number.isInteger(Number(v)) && v > 0
      },
      message: "The quantity is not an integer value",
    },
  },
},
{ timestamps: true })

module.exports = {
  CartItemSchema,
}
