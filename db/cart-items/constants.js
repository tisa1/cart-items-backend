const responses = {
  CART_ITEM_CREATED: "cartItemCreated",
  CART_ITEM_REMOVED: "cartItemRemoved",

  NO_PRODUCT_ID: "noProductId",
  PRODUCT_ID_INVALID: "productIdInvalid",
  NO_PRODUCT: "noProduct",

  NO_QUANTITY: "noQuantity",
  QUANTITY_INVALID: "quantityInvalid",

  NO_CART_ITEM_ID: "noCartItemId",
  CART_ITEM_ID_INVALID: "cartItemIdInvalid",
  NO_CART_ITEM: "noCartItem",
}

module.exports = {
  responses,
}
