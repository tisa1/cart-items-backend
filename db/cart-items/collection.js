const mongoose = require("mongoose")
const { CartItemSchema } = require("./schema")

const CartItem = mongoose.model("cart-items", CartItemSchema)

module.exports = { CartItem }
