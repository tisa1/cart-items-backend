const mongoose = require("mongoose")
const { CartItem } = require("db/cart-items/collection")
const User = require("db/users/collection")
const { Product } = require("db/products/collection")
const app = require("middleware/app")
const { responses } = require("db/cart-items/constants")
const jwtLib = require("jsonwebtoken")
const jwtSecurity = require("core/jwt")
const supertest = require("supertest")
const config = require("config")
const cartItemRouter = require("routes/cart-items/route")()

const request = supertest(app)

const createUser = async (data) => {
  if (!data) {
    data = {
      profile: {
        firstName: "Michael",
        lastName: "Perju",
      },
    }
  }

  return new User(data).save()
}

const login = async (user) => {
  if (!user) user = await createUser()
  return jwtLib.sign(
    { email: user.email, userId: user?._id },
    config.jwt.secret,
    {
      expiresIn: config.jwt.expiresIn,
    },
  )
}

const createProduct = async (data) => {
  if (!data) {
    data = {
      subcategoryId: "5f6c9a72a7985000298c24bc",
      organisationId: "5f6c9a72a7985000298c24bf",
      data: {
        randomField: "1",
        anotherRandomField: "1",
      },
    }
  }

  return new Product(data).save()
}

const createCartItem = async (user, product, quantity) => {
  if (!user) user = await createUser()
  if (!product) product = await createProduct()
  if (!quantity) quantity = 1

  return new CartItem({
    userId: user._id,
    productId: product._id,
    quantity: 1,
  }).save()
}

describe("cart-items me route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await CartItem.deleteMany({})
  })

  it("getting a user's cart-items should work", async () => {
    app.use("/", cartItemRouter)

    const user = await createUser()
    const token = await login(user)
    const product = await createProduct()
    await createCartItem(user, product)

    const res = await request.get("/me")
      .set("Authorization", `Bearer ${token}`)
      .expect(200)

    expect(res.body.cartItems.length).toBe(1)
    expect(res.body.total).toBe(1)

    // you should also get the product attached to it
    expect(res.body.cartItems[0].product).not.toBe(undefined)
  })

  it("getting user's cart-items without a token should not work", async () => {
    app.use("/", cartItemRouter)

    const user = await createUser()
    const product = await createProduct()
    await createCartItem(user, product)

    const res = await request.get("/me")
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.NO_TOKEN)
  })

  it("geting a user's cart-items with an invalid token should not work", async () => {
    app.use("/", cartItemRouter)

    const user = await createUser()
    const product = await createProduct()
    await createCartItem(user, product)

    const res = await request.get("/me")
      .set("Authorization", "Bearer XXXXXX")
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.TOKEN_INVALID)
  })
})
