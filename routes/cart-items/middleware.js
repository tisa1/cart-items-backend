const _ = require("underscore")
const { CartItem } = require("db/cart-items/collection")
const { responses } = require("db/cart-items/constants")

const optimizeFilters = async (req, __, next) => {
  // by default the filters are empty
  req.filters = {}

  return next()
}

const optimizeOptions = async (req, __, next) => {
  // by default the filters are empty
  req.options = {}

  const limit = Number(req?.query?.limit) || 10
  _.extend(req.options, { limit })

  const page = req?.query?.page || 1
  const skip = (page - 1) * limit
  _.extend(req.options, { skip })

  return next()
}

const checkIfShouldBeDeleted = async (req, res, next) => {
  const { productId, quantity } = req.body
  const { userId } = req.decoded

  const cartItem = await CartItem.findOne({ userId, productId })
  if (!cartItem) return next()

  if (cartItem.quantity === -Number(quantity)) {
    await CartItem.deleteOne({ productId, userId })
    return res.json({ message: responses.CART_ITEM_REMOVED })
  }

  return next()
}

module.exports = {
  optimizeFilters,
  optimizeOptions,
  checkIfShouldBeDeleted,
}
