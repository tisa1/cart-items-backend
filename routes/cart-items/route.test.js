const mongoose = require("mongoose")
const { CartItem } = require("db/cart-items/collection")
const User = require("db/users/collection")
const { Product } = require("db/products/collection")
const app = require("middleware/app")
const { responses } = require("db/cart-items/constants")
const jwtLib = require("jsonwebtoken")
const jwtSecurity = require("core/jwt")
const supertest = require("supertest")
const config = require("config")
const cartItemRouter = require("routes/cart-items/route")()

const request = supertest(app)

const createUser = async (data) => {
  if (!data) {
    data = {
      profile: {
        firstName: "Michael",
        lastName: "Perju",
      },
    }
  }

  return new User(data).save()
}

const login = async (user) => {
  if (!user) user = await createUser()
  return jwtLib.sign(
    { email: user.email, userId: user?._id },
    config.jwt.secret,
    {
      expiresIn: config.jwt.expiresIn,
    },
  )
}

const createProduct = async (data) => {
  if (!data) {
    data = {
      subcategoryId: "5f6c9a72a7985000298c24bc",
      organisationId: "5f6c9a72a7985000298c24bf",
      data: {
        randomField: "1",
        anotherRandomField: "1",
      },
    }
  }

  return new Product(data).save()
}

describe("cart-items route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await CartItem.deleteMany({})
  })

  it("creating a cart-item should work", async () => {
    app.use("/", cartItemRouter)

    const token = await login()
    const product = await createProduct()

    const res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
        quantity: "1",
      })
      .expect(200)

    expect(res.body.message).toBe(responses.CART_ITEM_CREATED)

    const cartItemsNo = await CartItem.countDocuments()
    expect(cartItemsNo).toBe(1)
  })

  it("creating a cart-item without a token should not work", async () => {
    app.use("/", cartItemRouter)

    const res = await request.post("/")
      .send({})
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.NO_TOKEN)
  })

  it("creating a cart-item with an invalid token should not work", async () => {
    app.use("/", cartItemRouter)

    const res = await request.post("/")
      .set("Authorization", "Bearer XXXXXX")
      .send({})
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.TOKEN_INVALID)
  })

  it("creating a cart-item without a product id should not work", async () => {
    app.use("/", cartItemRouter)

    const token = await login()

    const res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({})
      .expect(400)

    expect(res.body.message).toBe(responses.NO_PRODUCT_ID)
  })

  it("creating a cart-item with an invalid product id should not work", async () => {
    app.use("/", cartItemRouter)

    const token = await login()

    const res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: "fake",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.PRODUCT_ID_INVALID)
  })

  it("creating a cart-item with an unexisting product id should not work", async () => {
    app.use("/", cartItemRouter)

    const token = await login()

    const res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: "5f6c9a72a7985000298c24bc",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_PRODUCT)
  })

  it("creating a cart-item without quantity should not work", async () => {
    app.use("/", cartItemRouter)

    const token = await login()
    const product = await createProduct()

    const res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_QUANTITY)
  })

  it("creating a cart-item with an invalid quantity should not work", async () => {
    app.use("/", cartItemRouter)

    const token = await login()
    const product = await createProduct()

    let res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
        quantity: "0",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.QUANTITY_INVALID)

    res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
        quantity: 4.5,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.QUANTITY_INVALID)

    res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
        quantity: -2,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.QUANTITY_INVALID)
  })

  it("adding the same product to the cart should actually increase the cart-item quantity ", async () => {
    app.use("/", cartItemRouter)

    const token = await login()
    const product = await createProduct()

    let res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
        quantity: "4",
      })
      .expect(200)

    expect(res.body.message).toBe(responses.CART_ITEM_CREATED)

    res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
        quantity: "3",
      })
      .expect(200)

    expect(res.body.message).toBe(responses.CART_ITEM_CREATED)
    const cartItem = await CartItem.findOne()
    expect(cartItem.quantity).toBe(7)
  })

  it("adding the same product with a negative quantity to the cart should actually decrease the cart-item quantity ", async () => {
    app.use("/", cartItemRouter)

    const token = await login()
    const product = await createProduct()

    let res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
        quantity: "4",
      })
      .expect(200)

    expect(res.body.message).toBe(responses.CART_ITEM_CREATED)

    res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
        quantity: "-3",
      })
      .expect(200)

    expect(res.body.message).toBe(responses.CART_ITEM_CREATED)

    const cartItem = await CartItem.findOne()
    expect(cartItem.quantity).toBe(1)
  })

  it("adding/subtracting the quantity should correspondingly add/subtract the quantity of the product in the database", async () => {
    app.use("/", cartItemRouter)

    const token = await login()
    const product = await createProduct()

    let res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
        quantity: "4",
      })
      .expect(200)

    expect(res.body.message).toBe(responses.CART_ITEM_CREATED)

    res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
        quantity: "-3",
      })
      .expect(200)

    expect(res.body.message).toBe(responses.CART_ITEM_CREATED)

    res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
        quantity: "2",
      })
      .expect(200)

    expect(res.body.message).toBe(responses.CART_ITEM_CREATED)

    res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
        quantity: "-1",
      })
      .expect(200)

    expect(res.body.message).toBe(responses.CART_ITEM_CREATED)

    const cartItem = await CartItem.findOne()
    expect(cartItem.quantity).toBe(2)
  })
  it("removing the same quantity from the cart-item should actually delete the whole cart item", async () => {
    app.use("/", cartItemRouter)

    const token = await login()
    const product = await createProduct()

    let res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
        quantity: "4",
      })
      .expect(200)

    expect(res.body.message).toBe(responses.CART_ITEM_CREATED)

    res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        productId: product._id,
        quantity: "-4",
      })
      .expect(200)

    expect(res.body.message).toBe(responses.CART_ITEM_REMOVED)

    const cartItems = await CartItem.countDocuments()
    expect(cartItems).toBe(0)
  })
})
