const mongoose = require("mongoose")
const { CartItem } = require("db/cart-items/collection")
const User = require("db/users/collection")
const { Product } = require("db/products/collection")
const app = require("middleware/app")
const { responses } = require("db/cart-items/constants")
const jwtLib = require("jsonwebtoken")
const jwtSecurity = require("core/jwt")
const supertest = require("supertest")
const config = require("config")
const cartItemRouter = require("routes/cart-items/route")()

const request = supertest(app)

const createUser = async (data) => {
  if (!data) {
    data = {
      profile: {
        firstName: "Michael",
        lastName: "Perju",
      },
    }
  }

  return new User(data).save()
}

const login = async (user) => {
  if (!user) user = await createUser()
  return jwtLib.sign(
    { email: user.email, userId: user?._id },
    config.jwt.secret,
    {
      expiresIn: config.jwt.expiresIn,
    },
  )
}

const createProduct = async (data) => {
  if (!data) {
    data = {
      subcategoryId: "5f6c9a72a7985000298c24bc",
      organisationId: "5f6c9a72a7985000298c24bf",
      data: {
        randomField: "1",
        anotherRandomField: "1",
      },
    }
  }

  return new Product(data).save()
}

const createCartItem = async (user, product, quantity) => {
  if (!user) user = await createUser()
  if (!product) product = await createProduct()
  if (!quantity) quantity = 1

  return new CartItem({
    userId: user._id,
    productId: product._id,
    quantity: 1,
  }).save()
}

describe("cart-items delete route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await CartItem.deleteMany({})
  })

  it("deleting a cart-item should work", async () => {
    app.use("/", cartItemRouter)

    const user = await createUser()
    const token = await login(user)
    const product = await createProduct()
    const cartItem = await createCartItem(user, product)

    const res = await request.delete("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        cartItemId: cartItem._id,
      })
      .expect(200)

    expect(res.body.message).toBe(responses.CART_ITEM_REMOVED)

    const cartItemsNo = await CartItem.countDocuments()
    expect(cartItemsNo).toBe(0)
  })

  it("deleting a cart-item without a token should not work", async () => {
    app.use("/", cartItemRouter)

    const user = await createUser()
    const product = await createProduct()
    const cartItem = await createCartItem(user, product)

    const res = await request.delete("/")
      .send({
        cartItemId: cartItem._id,
      })
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.NO_TOKEN)

    const cartItemsNo = await CartItem.countDocuments()
    expect(cartItemsNo).toBe(1)
  })

  it("deleting a cart-item with an invalid token should not work", async () => {
    app.use("/", cartItemRouter)

    const user = await createUser()
    const product = await createProduct()
    const cartItem = await createCartItem(user, product)

    const res = await request.delete("/")
      .set("Authorization", "Bearer XXXXXX")
      .send({
        cartItemId: cartItem._id,
      })
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.TOKEN_INVALID)

    const cartItemsNo = await CartItem.countDocuments()
    expect(cartItemsNo).toBe(1)
  })

  it("deleting a cart-item without a cart item id should not work", async () => {
    app.use("/", cartItemRouter)

    const user = await createUser()
    const token = await login(user)
    const product = await createProduct()
    await createCartItem(user, product)

    const res = await request.delete("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_CART_ITEM_ID)

    const cartItemsNo = await CartItem.countDocuments()
    expect(cartItemsNo).toBe(1)
  })

  it("deleting a cart-item with an invalid cart item id should not work", async () => {
    app.use("/", cartItemRouter)

    const user = await createUser()
    const token = await login(user)
    const product = await createProduct()
    await createCartItem(user, product)

    const res = await request.delete("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        cartItemId: "fake",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.CART_ITEM_ID_INVALID)

    const cartItemsNo = await CartItem.countDocuments()
    expect(cartItemsNo).toBe(1)
  })

  it("deleting someone else's cart-item should not work", async () => {
    app.use("/", cartItemRouter)

    const user1 = await createUser()
    const user2 = await createUser()
    const token = await login(user2)
    const product = await createProduct()
    const cartItem = await createCartItem(user1, product)

    const res = await request.delete("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        cartItemId: cartItem._id,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_CART_ITEM)

    const cartItemsNo = await CartItem.countDocuments()
    expect(cartItemsNo).toBe(1)
  })
})
