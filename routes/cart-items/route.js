const mongoose = require("mongoose")
const express = require("express")
const errorService = require("lib/errors/handler")
const jwt = require("core/jwt")
const { responses } = require("db/cart-items/constants")

const { CartItem } = require("db/cart-items/collection")
const routeSecurity = require("./security")
const middleware = require("./middleware")

const createRouter = () => {
  const cartItemRouter = express.Router()

  // TODO: secure route creation
  cartItemRouter.post("/", [
    jwt.checkToken,
    routeSecurity.create,
    middleware.checkIfShouldBeDeleted,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const { productId, quantity } = req.body
      const { userId } = req.decoded

      await CartItem.updateOne({ userId, productId }, {
        $inc: {
          quantity,
        },
      }, { upsert: true })

      return res.json({ message: responses.CART_ITEM_CREATED })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on cart-items /create route",
      })
    }
  })

  cartItemRouter.get("/me", [
    jwt.checkToken,
    routeSecurity.me,
  ], async (req, res, next) => {
    try {
      const { userId } = req?.decoded
      const projection = {
        name: 1,
      }

      const cartItems = await CartItem.aggregate([
        {
          $match: {
            userId: mongoose.Types.ObjectId(userId),
          },
        },

        {
          $lookup: {
            from: "products",
            let: { productId: "$productId" },
            pipeline: [
              { $match: { $expr: { $eq: ["$_id", "$$productId"] } } },
              {
                $project: {
                  data: 1,
                },
              },
            ],
            as: "product",
          },
        },
        {
          $addFields: {
            product: { $arrayElemAt: ["$product", 0] },
          },
        },
      ])
      const total = await CartItem.countDocuments({ userId })

      return res.json({ cartItems, total })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on cart-items /me get route",
      })
    }
  })

  // cartItemRouter.get("/options", [
  // ], async (req, res, next) => {
  //   try {
  //     const projection = {
  //       name: 1,
  //     }

  //     const options = await CartItem.find({ cartItemId: null }, projection)

  //     return res.json({ options })
  //   } catch (error) {
  //     return errorService.handle({
  //       req, res, next, message: "Error on cart-items /options route",
  //     })
  //   }
  // })

  // cartItemRouter.get("/graph", [
  // ], async (req, res, next) => {
  //   try {
  //     const projection = {
  //       name: 1,
  //       cartItems: 1,
  //     }

  //     const cartItems = await CartItem.aggregate([
  //       {
  //         $match: {
  //           cartItemId: null,
  //         },
  //       },
  //       {
  //         $project: projection,
  //       },
  //     ])
  //     const total = await CartItem.countDocuments({ cartItemId: null })

  //     return res.json({ cartItems, total })
  //   } catch (error) {
  //     return errorService.handle({
  //       req, res, next, message: "Error on cart-items /graph route",
  //     })
  //   }
  // })

  cartItemRouter.delete("/", [
    jwt.checkToken,
    routeSecurity.remove,
  ], async (req, res, next) => {
    try {
      const cartItemId = req?.body?.cartItemId
      await CartItem.findByIdAndRemove(cartItemId)

      return res.json({ message: responses.CART_ITEM_REMOVED })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on cart-items / delete route",
      })
    }
  })

  return cartItemRouter
}

module.exports = createRouter
