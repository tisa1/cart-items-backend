const mongoose = require("mongoose")
const { CartItem } = require("db/cart-items/collection")
const { responses } = require("db/cart-items/constants")
const { Product } = require("db/products/collection")

const create = async (req, res, next) => {
  const userId = req?.decoded?.userId
  const productId = req?.body?.productId

  if (!productId) {
    return res.status(400).json({ message: responses.NO_PRODUCT_ID })
  }
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).json({ message: responses.PRODUCT_ID_INVALID })
  }

  const existingProduct = await Product.findById(productId, { _id: 1 })
  if (!existingProduct) {
    return res.status(400).json({ message: responses.NO_PRODUCT })
  }

  const quantity = req?.body?.quantity
  if (!quantity) {
    return res.status(400).json({ message: responses.NO_QUANTITY })
  }

  const existingCartItem = await CartItem.findOne({ userId, productId }, { quantity: 1 })
  const existingQuantity = existingCartItem?.quantity || 0

  if (!existingQuantity && !Number(quantity)) {
    return res.status(400).json({ message: responses.QUANTITY_INVALID })
  }

  if (!Number.isInteger(Number(quantity)) || existingQuantity + Number(quantity) < 0) {
    return res.status(400).json({ message: responses.QUANTITY_INVALID })
  }

  return next()
}

const me = async (req, res, next) => {
  // const userId = req?.decoded?.userId

  // if (!userId) {
  //   return res.status(400).json({ message: responses.NO_USER_ID })
  // }
  // if (!mongoose.Types.ObjectId.isValid(userId)) {
  //   return res.status(400).json({ message: responses.USER_ID_INVALID })
  // }
  // const existingCartItem = await User.findById(userId, { _id: 1 })
  // if (!existingCartItem) {
  //   return res.status(400).json({ message: responses.NO_USER_FOUND })
  // }

  return next()
}

const remove = async (req, res, next) => {
  const cartItemId = req?.body?.cartItemId
  const userId = req?.decoded?.userId

  if (!cartItemId) {
    return res.status(400).json({ message: responses.NO_CART_ITEM_ID })
  }
  if (!mongoose.Types.ObjectId.isValid(cartItemId)) {
    return res.status(400).json({ message: responses.CART_ITEM_ID_INVALID })
  }

  const existingCartItem = await CartItem.findOne({ _id: cartItemId, userId }, { _id: 1 })
  if (!existingCartItem) {
    return res.status(400).json({ message: responses.NO_CART_ITEM })
  }

  return next()
}

module.exports = {
  create,
  me,
  remove,
}
