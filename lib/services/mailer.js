const nodemailer = require("nodemailer")
const config = require("config")

const transporter = nodemailer.createTransport(config.smtp)

const sampleEmail = () => {
  const text = "Sample Text"

  const to = "test@app.com"
  const from = {
    name: "Backend",
    address: "team@backend",
  }
  const subject = "Subject"

  return transporter.sendMail({
    from, to, subject, text,
  })
}

module.exports = {
  sampleEmail,
}
