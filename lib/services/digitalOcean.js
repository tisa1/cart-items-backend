const AWS = require("aws-sdk")
const config = require("config")
const path = require("path")
const faker = require("faker")

const _ = require("underscore")

const spacesEndpoint = new AWS.Endpoint(config.digitalOceanSpaces.endpoint)
const s3 = new AWS.S3({
  endpoint: spacesEndpoint,
  accessKeyId: config.digitalOceanSpaces.accessKeyId,
  secretAccessKey: config.digitalOceanSpaces.secretAccessKey,
})

const getBuckets = () => new Promise((resolve, reject) => {
  s3.listBuckets({}, (err, data) => {
    if (err) {
      reject(err)
    } else {
      const buckets = data?.Buckets
      resolve(buckets)
    }
  })
})

const createBucket = () => {
  const params = {
    Bucket: config.digitalOceanSpaces.bucket,
  }
  return new Promise((resolve, reject) => {
    s3.createBucket(params, (err) => {
      if (err) {
        reject(err)
      }
      resolve()
    })
  })
}

const isKeyAlreadyTaken = (Key) => {
  const noSuchKeyCode = "NoSuchKey"

  const params = {
    Bucket: config.digitalOceanSpaces.bucket,
    Key,

  }

  return new Promise((resolve, reject) => {
    s3.getObject(params, (err) => {
      if (err) {
        if (err.code === noSuchKeyCode) {
          resolve(false)
        } else {
          reject(err)
        }
      }
      resolve(true)
    })
  })
}

const generateUniqueKey = async (Key) => {
  const parsedPath = path.parse(Key)
  const uuid = faker.datatype.uuid()

  const newKey = path.join(`${parsedPath.dir}`, `${parsedPath.name}.${uuid}${parsedPath.ext}`)

  if (await isKeyAlreadyTaken(newKey)) {
    return await generateUniqueKey(Key)
  }

  return newKey
}

const createUploadSignedUrl = async ({ Key, ContentType }) => {
  const Expires = 60 * 5

  Key = await generateUniqueKey(Key)

  const params = {
    Bucket: config.digitalOceanSpaces.bucket,
    Key,
    ContentType,
    Expires,
  }
  const signedUrl = s3.getSignedUrl("putObject", params)
  return { uploadId: Key, signedUrl }
}

const createDownloadSignedUrl = (Key) => {
  const Expires = 60 * 5

  return s3.getSignedUrl("getObject", {
    Bucket: config.digitalOceanSpaces.bucket,
    Key,
    Expires,
  })
}

const deleteFile = async (Key) => {
  const params = {
    Bucket: config.digitalOceanSpaces.bucket,
    Key,
  }

  return new Promise((resolve, reject) => {
    s3.deleteObject(params, (err) => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })
}

const uploadFile = async (Key, file) => {
  Key = await generateUniqueKey(Key)

  const params = {
    Bucket: config.digitalOceanSpaces.bucket,
    Body: file,
    ACL: "private",
    Key,
  }

  return new Promise((resolve, reject) => {
    s3.putObject(params, (err, data) => {
      if (err) reject(err)
      else resolve({ Key, ...data })
    })
  })
}

module.exports = {
  createUploadSignedUrl,
  createDownloadSignedUrl,
  isKeyAlreadyTaken,
  uploadFile,
  deleteFile,
  getBuckets,
  createBucket,
}
